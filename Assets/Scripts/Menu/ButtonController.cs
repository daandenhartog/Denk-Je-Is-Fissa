﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ButtonController : MonoBehaviour {

    public List<Transform> entries = new List<Transform>();

    public Color defaultColor = Color.black;
    public Color selectedColor = Color.white;

    public AudioEvent audioEvent;
    public AudioEvent gameoverEvent;
    public AudioSource audioSource;

    private int index;
    private bool canInteract = true;

    public void OpenScene(string scene) {
        if (scene == "exit") {
            Application.Quit();
        } else {
            GoToScene(scene);
        }
    }

    public void SetIndex(int index) {
        if (!canInteract)
            return;

        entries[this.index].GetComponentInChildren<Text>().color = defaultColor;
        entries[index].GetComponentInChildren<Text>().color = selectedColor;
        entries[index].GetComponentInChildren<Button>().Select();

        this.index = index;

        if (audioEvent != null)
            PlayRandomAudio();
    }

    public void ResetGame(string scene) {
        if (!canInteract)
            return;

        Destroy(FindObjectOfType<PlayerData>().gameObject);
        canInteract = false;

        gameoverEvent.Play(audioSource);

        StartCoroutine(LoadScene(scene, 3.5f));
    }

    public void GoToScene(string scene) {     
        SceneManager.LoadScene(scene);
    }

    private void PlayRandomAudio() {
        audioEvent.PlayNext(audioSource);
    }

    private void OnEnable() {
        SetIndex(0);
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.UpArrow))
            SetIndex(GetNextIndex(index, -1));

        if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.DownArrow))
            SetIndex(GetNextIndex(index, 1));

        if (Input.GetKeyDown(KeyCode.Space))
            entries[index].GetComponentInChildren<Button>().onClick.Invoke();
    }

    private int GetNextIndex(int index, int modifier) {
        index += modifier;
        if (index < 0)
            return entries.Count - 1;

        if (index >= entries.Count)
            return 0;

        return index;
    }

    private IEnumerator LoadScene(string scene, float delay) {
        yield return new WaitForSeconds(delay);
        GoToScene(scene);
    }
}