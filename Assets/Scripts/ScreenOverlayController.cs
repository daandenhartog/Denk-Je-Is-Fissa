﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ScreenOverlayController : MonoBehaviour {

    public Image screenOverlay;
    private Player player;

    private void Awake() {
        player = FindObjectOfType<Player>();
        if(player != null)
            player.SetPause(true);

        StartCoroutine(UnpausePlayer());
        FadeOut();
    }

    public void FadeIn() {
        screenOverlay.color = Color.black.AdjustAlpha(0f);
        FadeOverlayAlpha(1f, 1f);
    }

    public void FadeOut() {
        screenOverlay.color = Color.black;
        FadeOverlayAlpha(0f, 1f);
    }

    public void CameraFlash() {
        screenOverlay.color = Color.white;
        FadeOverlayAlpha(0f, 1.0f, 1.5f);
    }

    private void FadeOverlayAlpha(float endValue, float time, float delay = 0f, Ease ease = Ease.Linear) {
        screenOverlay.DOKill();
        screenOverlay.DOFade(endValue, time).SetDelay(delay).SetEase(ease);
    }

    private IEnumerator UnpausePlayer() {
        yield return new WaitForSeconds(1f);

        if(player != null)
            player.SetPause(false);
    }
}