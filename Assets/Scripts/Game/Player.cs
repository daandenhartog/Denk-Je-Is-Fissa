﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public Transform pivot;
    public Rigidbody2D rigidBody;
    public BalanceMeter balanceMeter;
    public PlayerView playerView;

    public string pickupIdentifier;
    public bool isPaused;

    public GameObject gameover;

    private Vector2 direction = Vector2.right;

    public int health = 0;
    public Achievement fallen;

    private float movementSpeed = 2f;
    private const float movementSpeedFluctuating = 0.5f;
    private const float movementSpeedFluctuatingModifier = 2.5f;

    private void Awake() {
        balanceMeter = new BalanceMeter();
        balanceMeter.ResetMeter();
    }

    private void Start() {
        health = FindObjectOfType<PlayerData>().GetHealth();
        playerView.SetHair(health);
    }

    public void SetPause(bool value) {
        isPaused = value;
        if (value)
            rigidBody.velocity = Vector2.zero;
        playerView.SetPause(isPaused);
    }

    public void SetPauseAll(bool value) {
        SetPause(value);

        foreach (Enemy e in FindObjectsOfType<Enemy>())
            e.SetPause(value);
    }

    public void ResetHealth() {
        health = 0;
        playerView.SetHair(health);
        balanceMeter.ResetMeter();
    }

    public int GetTimesFallen() {
        return PlayerPrefs.HasKey("fallen") ? PlayerPrefs.GetInt("fallen") : 0;
    }

    public void SetSpeed(float speed) {
        movementSpeed = speed;
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            Debug.Log(!gameover.activeSelf);
            bool toggle = !gameover.activeSelf;
            gameover.SetActive(toggle);
            SetPauseAll(toggle);
        }

        if (isPaused)
            return;

        UpdateBalanceMeter();
        UpdateMovement();

        if (balanceMeter.isDone)
            Fall();
    }

    private void UpdateBalanceMeter() {
        if (!balanceMeter.isDone)
            balanceMeter.Update();
    }

    public void Fall() {
        SetPause(true);
        health++;

        if(health < 5) {
            playerView.SetHair(health);
            playerView.SetFallen();

            StartCoroutine(StandUp());
        } else {
            playerView.SetDeath();
            gameover.SetActive(true);
        }

        int timesFallen = GetTimesFallen() + 1;
        PlayerPrefs.SetInt("fallen", timesFallen);

        if (timesFallen >= 420)
            FindObjectOfType<AchievementUnlocker>().UnlockAchievement(fallen, 0f);
    }

    private IEnumerator StandUp() {
        yield return new WaitForSeconds(2f);
        balanceMeter.ResetMeter();
        playerView.SetStand();
        SetPause(false);
    }

    private void UpdateMovement() {
        //if (Input.GetKeyDown(KeyCode.A)) direction = Vector2.left;
        if (Input.GetKeyDown(KeyCode.D)) direction = Vector2.right;
        if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.DownArrow)) {
            if (Input.GetKey(KeyCode.UpArrow)) direction.y = Vector2.up.y;
            if (Input.GetKey(KeyCode.DownArrow)) direction.y = Vector2.down.y;
        } else {
            direction.y = 0f;
        }

        //float speedModifier = Mathf.Sin(Time.timeSinceLevelLoad * movementSpeedFluctuatingModifier) * movementSpeedFluctuating;
        float speedModifier = 0f;
        
        Vector3 predictedPosition = (Vector3)transform.position + (Vector3)direction * (movementSpeed + speedModifier);
        if (predictedPosition.y < -5.5f) predictedPosition.y = -5.5f;
        if (predictedPosition.y > 0f) predictedPosition.y = 0f;

        //rigidBody.MovePosition(predictedPosition);
        rigidBody.velocity = predictedPosition - transform.position;
        transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.y);
        //transform.position = predictedPosition;
    }
}