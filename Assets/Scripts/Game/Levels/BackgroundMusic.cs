﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMusic : MonoBehaviour {

    public AudioClip[] clips;

    private AudioSource source;

    private void Awake() {
        source = GetComponent<AudioSource>();

        source.clip = clips[Random.Range(0, clips.Length)];
        source.Play();
    }
}