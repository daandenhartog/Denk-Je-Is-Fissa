﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy01 : TriggerObject {

    public ScreenOverlayController screenOverlay;

    private void Awake() {
        int timesFallen = PlayerPrefs.HasKey("fallen") ? PlayerPrefs.GetInt("fallen") : 0;
        PlayerPrefs.SetInt("timesFallen", timesFallen);
    }

    protected override void OnEnter() {
        base.OnEnter();
        
        GetComponent<Animator>().Play("Attack");
        GetComponent<AudioSource>().PlayDelayed(0.75f);

        StartCoroutine(InvokeFlash(0.8f));
    }

    private IEnumerator InvokeFlash(float delay) {
        yield return new WaitForSeconds(delay);
        screenOverlay.CameraFlash();
    }
}