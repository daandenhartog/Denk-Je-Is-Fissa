﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiscoMat : MonoBehaviour {

    public ScreenshakeController screenshake;
    public Player player;
    public AchievementUnlocker achievementUnlocker;
    public Achievement achievement;

    private int timesFallen;

    private void OnTriggerEnter2D(Collider2D col) {
        if (col.GetComponent<Player>() == null)
            return;

        timesFallen = player.GetTimesFallen();
        screenshake.FadeIn();
    }

    private void OnTriggerExit2D(Collider2D col) {
        if (col.GetComponent<Player>() == null)
            return;

        screenshake.FadeOut();

        if(timesFallen == player.GetTimesFallen()) {
            achievementUnlocker.UnlockAchievement(achievement, 0.5f);
        }
    }
}