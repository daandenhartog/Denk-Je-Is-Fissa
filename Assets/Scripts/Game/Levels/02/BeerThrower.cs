﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class BeerThrower : MonoBehaviour {

    public Animator animator;
    public Achievement achievement;

    public RectTransform beerTransform;
    public Image beerImage;

    public AudioSource source;

    private bool hasThrown = false;
    
    private void OnTriggerEnter2D(Collider2D col) {
        if (col.GetComponent<Player>() == null && !hasThrown)
            return;

        SetAttack();
    }

    private void SetAttack() {
        animator.Play("Attack");
        source.Play();

        beerTransform.DOScale(Vector3.one * 9.8f, 0.5f).SetDelay(1f);
        beerImage.DOColor(Color.white.AdjustAlpha(0f), 8f).SetDelay(1.5f);

        FindObjectOfType<AchievementUnlocker>().UnlockAchievement(achievement, 2f);

        hasThrown = true;
    }
}
