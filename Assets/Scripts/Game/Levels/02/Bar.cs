﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bar : MonoBehaviour {

    public GameObject popup;
    public AudioSource source;

    private bool isActive = false;
    private bool isFinished = false;

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Space) && isActive && !isFinished) {
            FindObjectOfType<Player>().ResetHealth();
            FindObjectOfType<Player>().SetPause(true);
            popup.SetActive(true);
            source.Play();
            isActive = false;
            isFinished = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D col) {
        if (col.GetComponent<Player>() == null)
            return;

        isActive = true;
    }

    private void OnTriggerExit2D(Collider2D col) {
        if (col.GetComponent<Player>() == null)
            return;

        isActive = false;
    }
}
