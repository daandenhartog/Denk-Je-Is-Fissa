﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : TriggerObject {

    public List<Transform> waypoints;
    public float movementSpeed;

    private int index;
    private bool isPaused;

    protected override void OnEnter() {
        base.OnEnter();
        FindObjectOfType<Player>().Fall();
        GetComponent<AudioSource>().Play();
    }

    public void SetPause(bool value) {
        isPaused = value;
        GetComponentInChildren<Animator>().speed = value ? 0f : 1f;
    }

    private void Update() {
        if (waypoints == null || waypoints.Count <= 1 || isPaused)
            return;

        transform.position = Vector2.MoveTowards(transform.position, waypoints[index].position, movementSpeed * Time.deltaTime);
        transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.y);

        if(Vector2.Distance(transform.position, waypoints[index].position) <= 0.1f) {
            index++;
            if (index >= waypoints.Count)
                index = 0;
        }
    }
}