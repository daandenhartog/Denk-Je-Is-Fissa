﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup_FjiezeFur : Pickup {

    protected override void OnEnter() {
        base.OnEnter();
        Debug.Log("Entering pickup");
    }

    protected override void OnPickup() {
        Debug.Log("picking up pickup");

        FindObjectOfType<FjiezeFur>().GetComponent<Animator>().Play("Idle_NoPickup");
        GetComponentInChildren<SpriteRenderer>().enabled = true;

        base.OnPickup();
    }

}