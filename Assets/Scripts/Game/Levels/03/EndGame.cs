﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGame : TriggerObject {

    public Animator animatorNPC;

    public Achievement perfectHealth;
    public Achievement worstHealth;
    public Achievement noFall;
    public Achievement[] pickupAchievements;
    public AchievementUnlocker unlocker;

    protected override void OnEnter() {
        base.OnEnter();

        Destroy(FindObjectOfType<PlayerData>().gameObject);

        Player player = FindObjectOfType<Player>();
        player.SetPause(true);
        int unlockedAchievements = 0;

        if(player.health == 0) {
            unlocker.UnlockAchievement(perfectHealth, 0f);
            unlockedAchievements++;
        } else if (player.health == 4) {
            unlocker.UnlockAchievement(worstHealth, 0f);
            unlockedAchievements++;
        }

        animatorNPC.Play("GetIn");
        GetComponent<AudioSource>().Play();

        string pickup = player.pickupIdentifier;
        foreach(Achievement a in pickupAchievements)
            if(a.token == pickup) {
                unlocker.UnlockAchievement(a, unlockedAchievements * 1.5f);
                unlockedAchievements++;
            }

        int timesFallen = PlayerPrefs.HasKey("fallen") ? PlayerPrefs.GetInt("fallen") : 0;
        if(timesFallen == PlayerPrefs.GetInt("timesFallen")) {
            unlocker.UnlockAchievement(noFall, unlockedAchievements * 1.5f);
            unlockedAchievements++;
        }

        StartCoroutine(FadeInAndLoadLevel(4f + unlockedAchievements * 1.5f));
    }

    private IEnumerator FadeInAndLoadLevel(float delay) {
        yield return new WaitForSeconds(delay);
        FindObjectOfType<ScreenOverlayController>().FadeIn();
        StartCoroutine(LoadNextLevel());
    }

    private IEnumerator LoadNextLevel() {
        yield return new WaitForSeconds(2f);
        SceneManager.LoadScene("Outro_03");
    }
}