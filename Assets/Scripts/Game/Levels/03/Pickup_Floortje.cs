﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup_Floortje : Pickup {

    public AudioSource audioSource;
    public AudioClip pickup;

    private RangedFloat audioInterval = new RangedFloat(4f, 6f);
    private float timer;

    private Transform player;

    private void Awake() {
        player = FindObjectOfType<Player>().transform;
        timer = audioInterval.GetRandomValue();
    }
    
    protected override void OnPickup() {
        base.OnPickup();

        audioSource.Stop();
        audioSource.clip = pickup;
        audioSource.Play();
    }

    protected override void Update() {
        base.Update();

        timer -= Time.deltaTime;
        if(timer <= 0 && Mathf.Abs(transform.position.x - player.position.x) < 10f) {
            timer = audioInterval.GetRandomValue();
            audioSource.Play();
        }
    }
}