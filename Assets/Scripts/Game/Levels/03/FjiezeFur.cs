﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FjiezeFur : TriggerObject {

    public AudioClip[] clips;
    public float[] clipDurations;

    public GameObject pickup;

    public AudioSource audioSource;
    
    protected override void OnEnter() {
        if (isActive)
            return;

        FindObjectOfType<Player>().SetPause(true);
        GetComponent<Animator>().Play("Talk_00");

        StartCoroutine(SetClip(clips[0], 0f));
        StartCoroutine(SetClip(clips[1], clipDurations[0]));
        StartCoroutine(SetClip(clips[2], clipDurations[0] + clipDurations[1]));
        StartCoroutine(SetClip(clips[3], clipDurations[0] + clipDurations[1] + clipDurations[2]));
        StartCoroutine(UnpausePlayer(clipDurations[0] + clipDurations[1] + clipDurations[2]));

        isActive = false;
    }

    private IEnumerator SetClip(AudioClip clip, float delay) {
        yield return new WaitForSeconds(delay);
        audioSource.Stop();
        audioSource.clip = clip;
        audioSource.Play();
    }

    private IEnumerator UnpausePlayer(float delay) {
        yield return new WaitForSeconds(delay);
        pickup.SetActive(true);
        FindObjectOfType<Player>().SetPause(false);
    }
}