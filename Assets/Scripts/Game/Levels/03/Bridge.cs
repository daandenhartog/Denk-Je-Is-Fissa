﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bridge : TriggerObject {

    private Player player;

	private void Awake () {
        player = FindObjectOfType<Player>();
	}

    protected override void OnEnter() {
        player.SetSpeed(1f);
    }

    protected override void OnExit() {
        player.SetSpeed(2f);
    }
}