﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class AchievementUnlocker : MonoBehaviour {

    public GameObject achievementPrefab;

    public void UnlockAchievement(Achievement achievement, float delay) {
        if (PlayerPrefs.HasKey(achievement.token) && PlayerPrefs.GetInt(achievement.token) == 1)
            return;

        RectTransform r = Instantiate(achievementPrefab).GetComponent<RectTransform>();
        r.SetParent(FindObjectOfType<Canvas>().transform, false);

        r.GetChild(1).GetComponent<Image>().sprite = achievement.sprite;
        r.GetChild(2).GetComponent<Text>().text = "Unlocked: " + achievement.title;
        r.GetChild(3).GetComponent<Text>().text = achievement.description;

        r.DOAnchorPosY(0f, 1f).SetDelay(delay);
        r.DOAnchorPosY(150f, 1f).SetDelay(delay + 6f);

        StartCoroutine(DestroyAfterSeconds(r.gameObject, delay + 8f));

        PlayerPrefs.SetInt(achievement.token, 1);
    }

    private IEnumerator DestroyAfterSeconds(GameObject g, float seconds) {
        yield return new WaitForSeconds(seconds);
        Destroy(g);
    }
}