﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : TriggerObject {

    public Achievement achievement;

    protected override void OnEnter() {
        isActive = true;
    }

    protected override void OnExit() {
        isActive = false;
    }

    protected virtual void Update() {
        if (!isActive)
            return;

        if (Input.GetKeyDown(KeyCode.Space)) {
            OnPickup();
        }
    }

    protected virtual void OnPickup() {
        FindObjectOfType<Player>().pickupIdentifier = achievement == null ? "" : achievement.token;

        Transform pickupPivot = GameObject.Find("Pickup_Pivot").transform;

        pickupPivot.DestroyAllChildren();
        transform.parent = pickupPivot;
        transform.localPosition = Vector2.zero;
        transform.localEulerAngles = Vector3.zero;

        Destroy(GetComponent<BoxCollider2D>());
        Destroy(this);
    }
}