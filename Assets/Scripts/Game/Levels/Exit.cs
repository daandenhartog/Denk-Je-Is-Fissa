﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Exit : TriggerObject {

    public string nextScene;

    protected override void OnEnter() {
        FindObjectOfType<Player>().SetPause(true);
        FindObjectOfType<PlayerData>().SetHealth(FindObjectOfType<Player>().health);

        FadeInAndLoadLevel();
    }

    private void FadeInAndLoadLevel() {
        FindObjectOfType<ScreenOverlayController>().FadeIn();

        StartCoroutine(LoadNextLevel());
    }

    private IEnumerator LoadNextLevel() {
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(nextScene);
    }

    private IEnumerator WaitForAchievements() {
        yield return new WaitForSeconds(1f);
    }
}