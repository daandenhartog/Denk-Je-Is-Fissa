﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Popup : MonoBehaviour {

    public Button button;
    public AudioClip clip;

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Space)) {
            button.onClick.Invoke();
        }
    }

    public void Buy() {
        FindObjectOfType<Player>().SetPause(false);
        gameObject.SetActive(false);
        AudioSource source = FindObjectOfType<Bar>().gameObject.GetComponent<AudioSource>();
        source.clip = clip;
        source.Play();
    }
}