﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PlayerView : MonoBehaviour {

    public Canvas canvas;

    public Transform bodyPivot;
    public Transform armLeftPivot, armRightPivot;
    public Transform balancePivot;
    public RectTransform balancePivotUI, balanceArrow;

    public GameObject bodyParent, legs, fallen, dead;
    public SpriteRenderer body, hair;

    public GameObject deathUI;

    public Image bodyUI;
    public Image hairUI;
    public Text live;

    public Sprite[] hairSprites;
    public Sprite[] bodySprites;

    public AudioEvent audioPlayerUI;
    public AudioEvent audioPlayerFall;
    public AudioEvent audioPlayerDeath;
    public AudioSource audioSource;

    public Animator legsAnimator;

    private RangedFloat rangeAnimateUI = new RangedFloat(5f, 9f);
    private float timerAnimateUI;
    private Player player;

    private void Awake() {
        player = GetComponent<Player>();
        live.DOColor(Color.white.AdjustAlpha(0f), 2f).SetEase(Ease.InExpo).SetLoops(-1);

        balancePivotUI.position = WorldToCanvas(canvas, balancePivot.position, Vector2.up / 2f);
        timerAnimateUI = rangeAnimateUI.GetRandomValue();
    }

    public void SetPause(bool value) {
        legsAnimator.speed = value ? 0f : 1f;
    }

    public void SetHair(int index) {
        hair.sprite = hairSprites[index];
        hairUI.sprite = hairSprites[index];
    }

    public void SetStand() {
        legs.SetActive(true);
        fallen.SetActive(false);
    }

    public void SetFallen() {
        audioPlayerFall.Play(audioSource);
        legs.SetActive(false);
        fallen.SetActive(true);
    }

    public void SetDeath() {
        audioPlayerDeath.Play(audioSource);
        bodyParent.SetActive(false);
        dead.SetActive(true);
        deathUI.SetActive(true);
    }

    private void Update() {
        if (player.balanceMeter.isDone || player.isPaused)
            return;
        
        balancePivotUI.position = WorldToCanvas(canvas, balancePivot.position, Vector2.up / 2f);
        float cubicLerp = (player.balanceMeter.valueCubic + 1) / 2f;
        balanceArrow.localEulerAngles = Vector3.forward * Mathf.Lerp(85, -85, cubicLerp);
        
        if (timerAnimateUI > 0f) {
            timerAnimateUI -= Time.deltaTime;
            if(timerAnimateUI <= 0f) {
                int index = 1 + Random.Range(0, 3);
                body.sprite = bodySprites[index];
                bodyUI.sprite = bodySprites[index];
                audioPlayerUI.Play(audioSource, index);
                StartCoroutine(SetBodyUINormal());
            }
        }

        float sin = Mathf.Sin(Time.time);
        float bodyLerp = (sin + 1f) / 2f;
        bodyPivot.localEulerAngles = Vector3.forward * Mathf.Lerp(-10, 10, bodyLerp);

        float rotationLerp = (player.balanceMeter.value + 1) / 2f;
        armLeftPivot.localEulerAngles = Vector3.forward * Mathf.Lerp(50f, -50f, rotationLerp); 
        armRightPivot.localEulerAngles = Vector3.forward * Mathf.Lerp(-50f, 50f, rotationLerp);
    }

    private IEnumerator SetBodyUINormal() {
        yield return new WaitForSeconds(3f);
        body.sprite = bodySprites[0];
        bodyUI.sprite = bodySprites[0];
        timerAnimateUI = rangeAnimateUI.GetRandomValue();
    }

    public static Vector2 WorldToCanvas(Canvas canvas, Vector2 position) { return WorldToCanvas(canvas, position, Vector2.zero); }
    public static Vector2 WorldToCanvas(Canvas canvas, Vector2 position, Vector2 offset) {
        Vector2 worldToScreenPosition = Camera.main.WorldToScreenPoint(position + offset);
        Vector2 screenToLocalPosition;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(canvas.transform as RectTransform, worldToScreenPosition, canvas.worldCamera, out screenToLocalPosition);
        return canvas.transform.TransformPoint(screenToLocalPosition);
    }

}