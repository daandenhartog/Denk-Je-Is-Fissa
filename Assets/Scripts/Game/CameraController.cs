﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public Transform target;
    public float leftBorder, rightBorder;
    public float movementSpeed = 2f;
    public float offset = 1.5f;

    private void LateUpdate() {
        Vector3 nextPosition = transform.position;
        nextPosition.x = target.position.x + offset;
        if (nextPosition.x < leftBorder) nextPosition.x = leftBorder;
        if (nextPosition.x > rightBorder) nextPosition.x = rightBorder;

        transform.position = Vector3.Lerp(transform.position, nextPosition, movementSpeed * Time.deltaTime);
    }
}