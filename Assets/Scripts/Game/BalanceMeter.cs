﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalanceMeter {

    public float value { get { return meter; } }
    public float valueCubic { get { return CubicEaseIn(meter); } }
    public bool isDone { get { return meter <= -1f || meter >= 1f; } }

    private float meter;
    private float timeUntilFull = 2f;

    public void ResetMeter() {
        meter = Random.Range(-0.1f, 0.1f);
    }

    public void Update() {
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow)) {
            meter += (1f / timeUntilFull) * (Input.GetKey(KeyCode.LeftArrow) ? -1 : 1) * Time.deltaTime;
        } else {
            meter += (1f / timeUntilFull) * (meter < 0f ? -1 : 1) * Time.deltaTime;
        }
    }

    private float CubicEaseIn(float value) {
        return value * value * value;
    }
}