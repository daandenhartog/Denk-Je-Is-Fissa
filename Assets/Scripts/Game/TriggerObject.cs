﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TriggerObject : MonoBehaviour {

    protected bool isActive;

    protected virtual void OnEnter() {

    }

    protected virtual void OnExit() {

    }

    private void OnTriggerEnter2D(Collider2D col) {
        if (col.GetComponent<Player>() == null)
            return;

        OnEnter();
    }

    private void OnTriggerExit2D(Collider2D col) {
        if (col.GetComponent<Player>() == null)
            return;

        OnExit();
    }
}