﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenshakeController : MonoBehaviour {

    public Screenshake screenshake;

    private int direction;
    private float timeCurrent;
    private const float timeUntilGoal = 1.0f;

    private bool isFinished { get { return (timeCurrent <= 0f && direction == -1) || (timeCurrent >= timeUntilGoal && direction == 1); } }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Alpha1))
            FadeIn();

        if (Input.GetKeyDown(KeyCode.Alpha2))
            FadeOut();

        if (isFinished)
            return;

        timeCurrent += (1f / timeUntilGoal) * direction * Time.deltaTime;
        screenshake.SetTrauma(timeCurrent / timeUntilGoal);
    }

    public void FadeIn() {
        timeCurrent = 0f;
        direction = 1;
    }

    public void FadeOut() {
        timeCurrent = timeUntilGoal;
        direction = -1;
    }
}