﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData : MonoBehaviour {
    
    private int health;

    private void Awake() {
        if (FindObjectsOfType<PlayerData>().Length > 1) {
            DestroyImmediate(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
    }

    public void SetHealth(int health) {
        this.health = health;
    }

    public int GetHealth() {
        return health;
    }
}