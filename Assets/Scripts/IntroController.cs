﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class IntroController : MonoBehaviour {

    public List<VideoClip> clips;
    public VideoPlayer player;
    public string nextScene;

    private int index;

    private void Awake() {
        player.loopPointReached += NextVideo;
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Escape))
            NextVideo(null);
    }

    private void NextVideo(VideoPlayer videoPlayer) {
        index++;

        if(index < clips.Count) {
            player.Stop();
            player.clip = clips[index];
            player.Play();
        } else {
            NextScene();
        }
    }

    private void NextScene() {
        SceneManager.LoadScene(nextScene);
    }
}