﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Video;

public class UnlockController : MonoBehaviour {

    public List<Unlock> unlocks = new List<Unlock>();
    public List<Transform> unlockButtons = new List<Transform>();
    public List<VideoClip> clips = new List<VideoClip>(); 

    public Sprite unlockedSprite;

    public void Awake() {
        for (int i = 0; i < unlockButtons.Count; i++) {
            if (IsUnlocked(i)) {
                SpriteState spriteState = unlockButtons[i].GetComponentInChildren<Button>().spriteState;
                spriteState.highlightedSprite = spriteState.pressedSprite = unlockedSprite;
                unlockButtons[i].GetComponentInChildren<Button>().spriteState = spriteState;
            }
                        
            unlockButtons[i].GetComponentInChildren<Text>().text = string.Format("\"{0}\"", unlocks[i].description);
        }
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Escape))
            SceneManager.LoadScene("Menu");
    }

    public void OpenUnlock(int index) {
        if (!IsUnlocked(index))
            return;

        SceneManager.LoadScene(string.Format("Unlock_{0}", index));
    }

    private bool IsUnlocked(int index) {
        return IsUnlocked(unlocks[index]);
    }

    private bool IsUnlocked(Unlock unlock) {
        if (PlayerPrefs.HasKey(unlock.token) && PlayerPrefs.GetInt(unlock.token) == 1)
            return true;

        return false;
    }
}