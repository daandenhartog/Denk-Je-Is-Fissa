﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Unlock", menuName = "Unlock")]
public class Unlock : ScriptableObject {

    public string token;
    public string description;

}