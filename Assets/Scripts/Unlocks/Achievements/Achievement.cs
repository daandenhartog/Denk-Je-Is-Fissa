﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Achievement", menuName = "Achievement")]
public class Achievement : Unlock {

    public Sprite sprite;
    public string title;
    public string unlocks;

}