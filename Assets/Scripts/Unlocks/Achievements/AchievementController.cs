﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AchievementController : MonoBehaviour {

    public List<Achievement> achievements;
    public List<Transform> achievementTransforms;

    private void Awake() {
        for(int i = 0; i < achievementTransforms.Count; i++) {
            bool unlocked = IsUnlocked(i);

            if(unlocked)
                achievementTransforms[i].GetChild(0).GetComponent<Image>().sprite = achievements[i].sprite;

            Color textColor = unlocked ? Color.white : Color.black;

            Text title = achievementTransforms[i].GetChild(1).GetChild(0).GetComponent<Text>();
            title.text = achievements[i].title;
            title.color = textColor;

            Text description = achievementTransforms[i].GetChild(1).GetChild(1).GetComponent<Text>();
            description.text = achievements[i].description + "\n" + achievements[i].unlocks;
            description.color = textColor;
        }
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Escape))
            SceneManager.LoadScene("Menu");
    }

    private bool IsUnlocked(int index) {
        return IsUnlocked(achievements[index]);
    }

    private bool IsUnlocked(Achievement achievement) {
        if (PlayerPrefs.HasKey(achievement.token) && PlayerPrefs.GetInt(achievement.token) == 1)
            return true;

        return false;
    }
}