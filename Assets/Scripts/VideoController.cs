﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class VideoController : MonoBehaviour {

    public VideoPlayer video;
    public string nextScene;

    private void Awake() {
        video.loopPointReached += GoToScene;
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Escape))
            GoToScene(null);
    }

    private void GoToScene(VideoPlayer videoPlayer) {
        SceneManager.LoadScene(nextScene);
    }
}