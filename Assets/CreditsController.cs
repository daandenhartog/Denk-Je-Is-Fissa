﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class CreditsController : MonoBehaviour {

    public RectTransform credits;

    private void Awake() {
        credits.DOAnchorPos(Vector2.up * 1650f, 60f);
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Escape) || credits.anchoredPosition.y >= 1600)
            GoToNextScene();
    }

    private void GoToNextScene() {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Menu");
    }
}